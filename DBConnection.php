<?php 
    function OpenConnection()
    {
        $dbHost = "localhost";
        $dbUser = "root";
        $dbPassword = "";
        $dbName = "information_system";
        $port = "3306";

        $conn = new mysqli($dbHost, $dbUser, 
        $dbPassword, $dbName, $port) or die("Connection Failed: %s\n".$conn->error);
        return $conn;
    }

    function CloseConnection($conn)
    {
        $conn -> close();
    }
?>