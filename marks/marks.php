<?php 
  session_start();
  if(!isset($_SESSION['username']) || $_SESSION['role']!="admin"){
    header("location:../index.php"); 
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Examination Marks</title>
    <script defer src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script defer src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script defer src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>

<body>
    <?php 
        include "../DBConnection.php";
        $conn = OpenConnection();
        $sql = "SELECT RegNum, FirstName, LastName FROM `STUDENT`";
        $results = $conn -> query($sql);
        $query = "SELECT * FROM UNIT";
        $unitResults = $conn -> query($query);
        CloseConnection($conn);
    ?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../home.php">Home</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Register
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../registration/registration.php">Student</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="../course/course.php">Course</a>
                        <a class="dropdown-item" href="../unit/unit.php">Unit</a>

                    </div>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="../marks/marks.php">Marks <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="jumbotron">
        <h4>Marks Entry</h4>
    </div>
    <div class="container">
        <form action="grade.php" method="post">
            <div class="form-row">
                <div class="col">
                    <label for="UnitCode">Unit</label>
                    <select id="UnitCode" name="UnitCode" class="form-control">
                        <option selected disabled>Choose...</option>
                        <?php
                        while($row = mysqli_fetch_array($unitResults)){
                            echo "<option value='" . $row['UnitCode'] . "'>" . $row['UnitCode']." (".$row['UnitName'].")" . "</option>";
                        }
                    ?>
                    </select>
                </div>
            </div>
            <br>
            <div class="form-row">
                <label for="regNum">Student Registration Number</label>
                <select id="regNum" name="regNum" class="form-control">
                    <option selected disabled>Choose...</option>
                    <?php
                        while($row = mysqli_fetch_array($results)){
                            echo "<option value='" . $row['RegNum'] . "'>" . $row['RegNum']." (".$row['FirstName']." ".$row['LastName'].")" . "</option>";
                        }
                    ?>
                </select>
            </div>
            <br>
            <div class="form-row">
                <div class="col">
                    <label for="catMarks">Cat Marks</label>
                    <input type="number" name="catMarks" id="catMarks" class="form-control" placeholder="30" required
                        min="0" max="30">
                </div>
                <div class="col">
                    <label for="examMarks">Examination Marks</label>
                    <input type="number" name="examMarks" id="examMarks" class="form-control" placeholder="70" required
                        min="0" max="70">
                </div>
            </div>
            <br>
            <input type="submit" value="Submit" class="btn btn-success">
        </form>
    </div>
</body>

</html>