-- This file is meant to create the tables to be used to run this application
-- Tables are created in the respective order used below

-- COURSES Table
CREATE TABLE `course` (
  `CourseId` int(100) NOT NULL,
  `CourseName` varchar(30) NOT NULL,
  PRIMARY KEY (`CourseName`),
  UNIQUE KEY `CourseId` (`CourseId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- UNIT Table
CREATE TABLE `unit` (
  `UnitCode` varchar(8) NOT NULL,
  `UnitName` varchar(30) NOT NULL,
  PRIMARY KEY (`UnitCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- USERS Table
CREATE TABLE `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `User_Type` varchar(100) NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- STUDENT Table
CREATE TABLE `student` (
  `RegNum` varchar(100) NOT NULL,
  `FirstName` varchar(30) NOT NULL,
  `LastName` varchar(30) NOT NULL,
  `CourseName` varchar(30) NOT NULL,
  `RegDate` date NOT NULL,
  PRIMARY KEY (`RegNum`),
  KEY `CourseName` (`CourseName`),
  CONSTRAINT `STUDENT_ibfk_1` FOREIGN KEY (`CourseName`) REFERENCES `course` (`CourseName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- EXAMINATION Table
CREATE TABLE `examination` (
  `ExamId` int(11) NOT NULL AUTO_INCREMENT,
  `UnitCode` varchar(8) NOT NULL,
  `RegNum` varchar(100) NOT NULL,
  `UnitName` varchar(30) NOT NULL,
  `CatMarks` int(2) NOT NULL,
  `ExamMarks` int(2) NOT NULL,
  `Total` int(3) NOT NULL,
  `Grade` char(1) NOT NULL,
  PRIMARY KEY (`UnitCode`,`RegNum`,`UnitName`),
  UNIQUE KEY `ExamId` (`ExamId`,`UnitName`),
  KEY `UnitCode` (`UnitCode`,`RegNum`),
  KEY `RegNum` (`RegNum`),
  CONSTRAINT `EXAMINATION_ibfk_1` FOREIGN KEY (`UnitCode`) REFERENCES `unit` (`UnitCode`),
  CONSTRAINT `EXAMINATION_ibfk_2` FOREIGN KEY (`RegNum`) REFERENCES `student` (`RegNum`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;

-- INSERTING DEFAULT USERS
INSERT INTO `users` (`uid`, `Username`, `Password`, `User_Type`, `Created`) 
VALUES 
(1, 'student', md5('student'), 'student', curdate()),
(2, 'admin', md5('admin'), 'admin', CURDATE());