<?php 
  session_start();
  if(!isset($_SESSION['username']) || $_SESSION['role']!="student"){
    header("location:../index.php"); 
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student Grades</title>
    <script defer src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script defer src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script defer src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>

<body>
    <?php 
        include "../DBConnection.php";
        $conn = OpenConnection();
        $regNum = $_POST["regNum"];
        $sql = "SELECT * FROM `EXAMINATION` WHERE RegNum = '$regNum'";
        $results = $conn -> query($sql);
        CloseConnection($conn);
    ?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <a class="navbar-brand" href="home.php">University</a>
                <a href="../logout.php"><button class="btn btn-outline-danger my-2 my-sm-0"
                        type="submit">Logout</button></a>
        </div>
    </nav>
    <div class="jumbotron">
        <h4><?php echo "Marks For: ".$regNum ?></h4>
    </div>
    <div class="container">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Unit Code</th>
                    <th scope="col">Unit Name</th>
                    <th scope="col">Cat Marks</th>
                    <th scope="col">Examination Marks</th>
                    <th scope="col">Total Marks</th>
                    <th scope="col">Grade</th>
                </tr>
            </thead>
            <tbody>
                <?php
        while($row = mysqli_fetch_array($results)){?>
                <tr>
                    <td><?php echo $row['UnitCode'] ?></td>
                    <td><?php echo $row['UnitName'] ?></td>
                    <td><?php echo $row['CatMarks'] ?></td>
                    <td><?php echo $row['ExamMarks'] ?></td>
                    <td><?php echo $row['Total'] ?></td>
                    <td><?php echo $row['Grade'] ?></td>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</body>

</html>