<!DOCTYPE html>
<html lang="en">
<?php
    session_start();
    include "DBConnection.php";
    $conn = OpenConnection();
    $msg="";
    if(isset($_POST['login'])){ // checking if login button has been clicked
        $username = $_POST['username'];
        $password = $_POST['password'];
        $password = sha1($password);
        $userType = $_POST['userType'];
        
        $sql = "SELECT * FROM USERS WHERE Username = ? AND Password = ? AND User_Type = ?";

        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sss",$username,$password,$userType);
        $stmt->execute();
        $result = $stmt -> get_result();
        $row = $result->fetch_assoc();

        session_regenerate_id();
        $_SESSION['username'] = $row['Username'];
        $_SESSION['role'] = $row['User_Type'];
        session_write_close();

        if($result->num_rows==1 && $_SESSION['role']=="student"){
            header("location:student/home.php");
        }else if($result->num_rows==1 && $_SESSION['role']=="admin"){
            header("location:home.php");
        }else{
            $msg="Username or Password is Invalid";
        }
    }
    CloseConnection($conn);
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <title>University Marks Registration</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script defer src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script defer src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script defer src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>
</head>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 mt-5 px-0">
                <h3 class="text-center text-light bg-primary p-3">University Login</h3>
                <form action="<?= $_SERVER['PHP_SELF']?>" method="POST" class="p-4">
                    <h5 class="text-danger text-center"><?php echo $msg; ?></h5>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control form-control-lg" required placeholder="Username" name="username">
                        <small class="form-text text-muted"><ul><li>Admin site username is "admin"</li><li>Student Site username is "student"</li></ul></small>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control form-control-lg" required placeholder="Password" name="password">
                        <small class="form-text text-muted"><ul><li>Admin site password is "admin"</li><li>Student Site password is "student"</li></ul></small>
                    </div>
                    <div class="form-group lead">
                        <label for="userTypr">I am a: </label>
                        <input type="radio" name="userType" class="custom-radio" value="admin" required> Admin  
                        <input type="radio" name="userType" class="custom-radio" value="student" required> Student
                    </div>
                    <div class="form-group">
                        <input type="submit" name="login" class="btn btn-primary btn-block">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>