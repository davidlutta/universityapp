<?php 
  session_start();
  if(!isset($_SESSION['username']) || $_SESSION['role']!="admin"){
    header("location:index.php"); 
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>University Marks Registration</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <script defer src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
  </script>
  <script defer src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
  </script>
  <script defer src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
    integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
  </script>
</head>

<body>
  <?php
        include "DBConnection.php";
        $conn = OpenConnection();
        $sql = "SELECT * FROM `STUDENT` ORDER BY RegDate ASC";
        $results = $conn -> query($sql);
        CloseConnection($conn);
    ?>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            Register
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="registration/registration.php">Student</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="course/course.php">Course</a>
            <a class="dropdown-item" href="unit/unit.php">Unit</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="marks/marks.php">Marks</a>
        </li>
      </ul>
      <div class="form-inline my-2 my-lg-0">
      <a href="logout.php"><button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Logout</button></a>
      </div>
    </div>
  </nav>
  <div class="jumbotron">
    <h4>Registered Students</h4>
  </div>
  <div class="container-fluid">
    <table class="table">
      <thead class="thead-dark">
        <tr>
          <th scope="col">Registration Number</th>
          <th scope="col">First Name</th>
          <th scope="col">Last Name</th>
          <th scope="col">Course Name</th>
          <th scope="col">Registration Date</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        <?php
        while($row = mysqli_fetch_array($results)){?>
        <tr>
          <td><?php echo $row['RegNum'] ?></td>
          <td><?php echo $row['FirstName'] ?></td>
          <td><?php echo $row['LastName'] ?></td>
          <td><?php echo $row['CourseName'] ?></td>
          <td><?php echo date_format(date_create($row['RegDate']), 'D d M Y') ?></td>
          <td>
            <form action="student/student.php" method="post">
              <input type="submit" class="btn btn-info" name="action" value="View Marks">
              <input type="hidden" name="id" value="<?php echo $row['RegNum']; ?>" />
            </form>
          </td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
</body>

</html>