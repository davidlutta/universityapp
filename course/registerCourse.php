<?php 
  session_start();
  if(!isset($_SESSION['username']) || $_SESSION['role']!="admin"){
    header("location:../index.php"); 
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Course Registration</title>
    <script defer src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script defer src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script defer src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>

<body>
    <?php
    include "../DBConnection.php";
    $conn = OpenConnection();

    $courseId = $_POST['CourseId'];
    $courseName = $_POST['CourseName'];
    $sql = "INSERT INTO `COURSE` (`CourseId`, `CourseName`) VALUES ('$courseId', '$courseName')";
    $msg='';
    $error='';
    if($conn -> query($sql) == TRUE){
        $msg = "Course Registered Successfully";
    }else{
        $error = "Error: ".$conn->error;
    }
    CloseConnection($conn);
    header("Refresh:3, url=../course/course.html"); // Will Refresh the page after 3 seconds
?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../home.php">Home</a>
                </li>
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Register
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../registration/registration.php">Student</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="../course/course.php">Course <span class="sr-only">(current)</span></a>
                        <a class="dropdown-item" href="../unit/unit.php">Unit</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../marks/marks.php">Marks</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="jumbotron">
        <h4>Course Registration</h4>
    </div>
    <div class="container">
        <?php if(strlen($msg)>0){ ?>
        <div class="alert alert-success" role="alert">
            <p class="font-weight-bold text-success"><?php echo $msg ?></p>
        </div>
        <?php }?>

        <?php if(strlen($error)>0){ ?>
        <div class="alert alert-danger" role="alert">
            <p class="font-weight-bold text-danger"><?php echo $error ?></p>
        </div>
        <?php }?>
    </div>
</body>

</html>